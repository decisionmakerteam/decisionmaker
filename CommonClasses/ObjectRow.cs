﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses
{
    /// <summary>
    /// Объект (строка в матрице)
    /// </summary>
    public class ObjectRow
    {
        /// <summary>
        /// Имя объекта
        /// </summary>
        private String name;
        /// <summary>
        /// Параметры объекта
        /// </summary>
        private List<double> properties;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя объекта</param>
        /// <param name="properties">Параметры</param>
        public ObjectRow(String name, List<double> properties)
        {
            this.name = name;
            this.properties = properties;
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя объекта</param>
        public ObjectRow(String name)
            : this(name, new List<double>())
        {
        }
        /// <summary>
        /// Добавить своство
        /// </summary>
        /// <param name="val">значение</param>
        public void AddProperty(double val)
        {
            properties.Add(val);
        }
        /// <summary>
        /// Получить все свойства
        /// </summary>
        public double[] Properties
        {
            get { return properties.ToArray(); }
        }
        /// <summary>
        /// Получить имя объекта
        /// </summary>
        public String Name
        {
            get { return name; }
        }
        /// <summary>
        /// Получить число свойств
        /// </summary>
        public int ColumnCount
        {
            get { return properties.Count; }
        }
        /// <summary>
        /// Задаёт значение свойства 
        /// </summary>
        /// <param name="index">Номер свойства</param>
        /// <param name="value">Новое значение</param>
        public void setProperty(int index, double value)
        {
            properties[index] = value;
        }
    }
}
