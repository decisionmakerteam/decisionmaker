﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses
{
    /// <summary>
    /// Перечисляемый вид данных
    /// </summary>
    public enum DataType { 
        /// <summary>
        /// Возрастающий
        /// </summary>
        Ascending, 
        /// <summary>
        /// Убывающий
        /// </summary>
        Descending, 
        /// <summary>
        /// Качественный
        /// </summary>
        Qualitative }
    /// <summary>
    /// Матрица с данными
    /// </summary>
    public class TaskMatrix
    {
        /// <summary>
        /// Объекты (строки)
        /// </summary>
        private List<ObjectRow> objects;
        /// <summary>
        /// Параметры (столбцы)
        /// </summary>
        private List<ObjectColumn> columns;
        /// <summary>
        /// Имя таблицы
        /// </summary>
        private String name;
        /// <summary>
        /// число столбцов
        /// </summary>
        int colCount;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя таблицы</param>
        /// <param name="colCount">Число столбцов</param>
        public TaskMatrix(String name, int colCount)
        {
            this.colCount = colCount;
            this.name = name;
            columns = new List<ObjectColumn>();
            objects = new List<ObjectRow>();
        }
        /// <summary>
        /// Добавить столбец в таблицу
        /// </summary>
        /// <param name="objectColumn">Столбец</param>
        /// <returns>Добавлен или нет</returns>
        public bool AddColumn(ObjectColumn objectColumn)
        {
            if (colCount > columns.Count)
            {
                columns.Add(objectColumn);
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// Добавить столбец
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="weight">Вес</param>
        /// <param name="dataType">Тип данных</param>
        /// <returns>Добавлен или нет</returns>
        public bool AddColumn(String name, double weight, DataType dataType)
        {
            return AddColumn(new ObjectColumn(name, weight, dataType));
        }
        /// <summary>
        /// Добавить объект
        /// </summary>
        /// <param name="objectRow">Объект</param>
        /// <returns>Добавлен или нет</returns>
        public bool AddRow(ObjectRow objectRow)
        {
            if (columns.Count == colCount && objectRow.Properties.Length==colCount)
            {
                objects.Add(objectRow);
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// Получить все столбцы
        /// </summary>
        public ObjectColumn[] Columns
        {
            get { return columns.ToArray(); }
        }
        /// <summary>
        /// Получить имя
        /// </summary>
        public String Name
        {
            get { return name; }
        }
        /// <summary>
        /// Получить объекты
        /// </summary>
        public ObjectRow[] Rows
        {
            get { return objects.ToArray(); }
        }
        /// <summary>
        /// Добавить первое значение в начало массива
        /// </summary>
        /// <param name="val">Первое значение</param>
        /// <param name="valArr">Массив значений</param>
        /// <returns>Обновленный массив</returns>
        private object[] AddBefore(object val, double[] valArr)
        {
            List<object> res = new List<object>();
            res.Add(val);
            res.AddRange(valArr.ToList().ConvertAll(x=>x.ToString()));
            return res.ToArray();
        }
        /// <summary>
        /// Добавить первое значение в начало массива
        /// </summary>
        /// <param name="val">Первое значение</param>
        /// <param name="valArr">Массив значений</param>
        /// <returns>Обновленный массив</returns>
        private object[] AddBefore(object val, String[] valArr)
        {
            List<object> res = new List<object>();
            res.Add(val);
            res.AddRange(valArr);
            return res.ToArray();
        }


        /// <summary>
        /// Перевести в DataTable
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable ToDataTable()
        {
            DataTable res = new DataTable(name);
            res.Columns.Add("Наименование");
            columns.ForEach(x=>res.Columns.Add(x.Name.Replace(" ","_")));
            res.Rows.Add(AddBefore("Тип критерия", columns.ConvertAll(x=>x.DataType==DataType.Qualitative?"Качественная":x.DataType==DataType.Ascending?"Возрастающая":"Убывающая").ToArray()));
            res.Rows.Add(AddBefore("Вес критерия", columns.ConvertAll(x=>x.Weight).ToArray()));
            objects.ForEach(x=>res.Rows.Add(AddBefore(x.Name,x.Properties)));
            return res;
        }
    }
}
