﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses
{
    /// <summary>
    /// Параметр объекта (столбец в таблице)
    /// </summary>
    public class ObjectColumn
    {
        /// <summary>
        /// Тип данных
        /// </summary>
        private DataType dataType;
        /// <summary>
        /// Вес
        /// </summary>
        private double weight;
        /// <summary>
        /// Имя
        /// </summary>
        private String name;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя объекта</param>
        /// <param name="weight">Вес объекта</param>
        /// <param name="dataType">Тип объекта</param>
        public ObjectColumn(String name, double weight, DataType dataType)
        {
            this.name = name;
            this.weight = weight;
            this.dataType = dataType;
        }
        /// <summary>
        /// Получить имя параметра
        /// </summary>
        public String Name
        {
            get { return name; }
        }
        /// <summary>
        /// Получить вес параметра
        /// </summary>
        public double Weight
        {
            get { return weight; }
        }
        /// <summary>
        /// Получить тип параметра
        /// </summary>
        public DataType DataType
        {
            get { return dataType; }
        }
    }
}
