using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace CommonClasses
{
    /// <summary>
    /// ������������� ��� ��� �������� ���� ���������
    /// </summary>
    public enum ParameterType { 
        /// <summary>
        /// ������ ����������
        /// </summary>
        Boolean, 
        /// <summary>
        /// ����� �����
        /// </summary>
        Integer, 
        /// <summary>
        /// �������������� �����
        /// </summary>
        Double };
    /// <summary>
    /// ����� - �������� ����������� �����
    /// </summary>
    public class Parameter
    {
        /// <summary>
        /// ������������ ��������� �� �����
        /// </summary>
        private String name;
        /// <summary>
        /// ������������ ��������� � ����� ������������ (������ ���� ���������!)
        /// </summary>
        private String configName;
        /// <summary>
        /// ��� ���������
        /// </summary>
        private ParameterType parameterType;
        /// <summary>
        /// ����������� �������� �������� �����
        /// </summary>
        private double minValDouble;
        /// <summary>
        /// ������������ �������� �������� �����
        /// </summary>
        private double maxValDouble;
        /// <summary>
        /// �������� �������� ����� �� ���������
        /// </summary>
        private double defaultValDouble;
        /// <summary>
        /// ����������� ��� ���������
        /// </summary>
        private double step;
        /// <summary>
        /// ����������� �������� ������ �����
        /// </summary>
        private int minValInt;
        /// <summary>
        /// ������������ �������� ������ �����
        /// </summary>
        private int maxValInt;
        /// <summary>
        /// �������� ������ ����� �� ���������
        /// </summary>
        private int defaultValInt;
        /// <summary>
        /// �������� ���������� ���������� �� ���������
        /// </summary>
        private bool deafaultValBool;
        /// <summary>
        /// ����������� �������������� ���������
        /// </summary>
        /// <param name="name">��������</param>
        /// <param name="maxVal">������������ ��������</param>
        /// <param name="minVal">����������� ��������</param>
        /// <param name="defaultVal">�������� �� ���������</param>
        /// <param name="configName">��� � ����� ������������</param>
        public Parameter(String name, int maxVal, int minVal, int defaultVal, string configName)
        {
            this.name = name;
            this.minValInt = minVal;
            this.maxValInt = maxVal;
            this.defaultValInt = defaultVal;
            this.parameterType = ParameterType.Integer;
            this.configName = configName;
        }
        /// <summary>
        /// ����������� ��������������� �����
        /// </summary>
        /// <param name="name">��������</param>
        /// <param name="maxVal">������������ ��������</param>
        /// <param name="minVal">����������� ��������</param>
        /// <param name="defaultVal">�������� �� ���������</param>
        /// <param name="step">��� ���������</param>
        /// <param name="configName">��� � ����� ������������</param>
        public Parameter(String name, double maxVal, double minVal, double defaultVal,double step, String configName)
        {
            this.name = name;
            this.parameterType = ParameterType.Double;
            this.maxValDouble = maxVal;
            this.minValDouble = minVal;
            this.step = step;
            this.defaultValDouble = defaultVal;
            this.configName = configName;
        }
        /// <summary>
        /// ����������� �������� ���������
        /// </summary>
        /// <param name="name">��������</param>
        /// <param name="defaultVal">�������� �� ���������</param>
        /// <param name="configName">��� � ����� ������������</param>
        public Parameter(String name, bool defaultVal, String configName)
        {
            this.name = name;
            this.parameterType = ParameterType.Boolean;
            this.deafaultValBool = defaultVal;
            this.configName = configName;
        }
        /// <summary>
        /// ��� ��������� (�����, ��������������, �������)
        /// </summary>
        public ParameterType ParameterType
        {
            get { return parameterType; }
        }
        /// <summary>
        /// �������� ���������
        /// </summary>
        public String Name
        {
            get { return name; }
        }
        /// <summary>
        /// ��� ��������� � ����� ������������
        /// </summary>
        public String ConfigurationName
        {
            get { return configName; }
        }
        /// <summary>
        /// ����������� ����� ��������
        /// </summary>
        public int MinimumIneteger
        {
            get { return minValInt; }
        }
        /// <summary>
        /// ������������ ����� ��������
        /// </summary>
        public int MaximumIneteger
        {
            get { return maxValInt; }
        }
        /// <summary>
        /// ����������� �������������� ��������
        /// </summary>
        public double MinimumDouble
        {
            get { return minValDouble; }
        }
        /// <summary>
        /// ������������ �������������� ��������
        /// </summary>
        public double MaximumDouble
        {
            get { return maxValDouble; }
        }
        /// <summary>
        /// ��� ���������
        /// </summary>
        public double Step
        {
            get { return step; }
        }
        /// <summary>
        /// ����� �������� �� ��������
        /// </summary>
        public int DefaultInteger
        {
            get { return defaultValInt; }
        }
        /// <summary>
        /// �������������� �������� �� ���������
        /// </summary>
        public double DefaultDouble
        {
            get { return defaultValDouble; }
        }
        /// <summary>
        /// ������� �������� �� ���������
        /// </summary>
        public bool DefaultBoolean
        {
            get { return deafaultValBool; }
        }
        /// <summary>
        /// ������ ��������
        /// </summary>
        public bool BooleanValue
        {
            get
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration("");
                if (config.AppSettings.Settings[configName] != null)
                {
                    return bool.Parse(config.AppSettings.Settings[configName].Value);
                }
                else
                {
                    config.AppSettings.Settings.Add(configName, DefaultBoolean.ToString());
                    config.Save(ConfigurationSaveMode.Modified);
                    return DefaultBoolean;
                }
            }
            set
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration("");
                if (config.AppSettings.Settings[configName] != null)
                {
                    config.AppSettings.Settings[configName].Value = value.ToString();
                }
                else
                {
                    config.AppSettings.Settings.Add(configName, value.ToString());
                }
                config.Save(ConfigurationSaveMode.Modified);
            }
        }
        /// <summary>
        /// �������������� ��������
        /// </summary>
        public double DoubleValue
        {
            get
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration("");
                if (config.AppSettings.Settings[configName] != null)
                {
                    return double.Parse(config.AppSettings.Settings[configName].Value);
                }
                else
                {
                    config.AppSettings.Settings.Add(configName, DefaultDouble.ToString());
                    config.Save(ConfigurationSaveMode.Modified);
                    return DefaultDouble;
                }
            }
            set
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration("");
                if (config.AppSettings.Settings[configName] != null)
                {
                    config.AppSettings.Settings[configName].Value = value.ToString();
                }
                else
                {
                    config.AppSettings.Settings.Add(configName, value.ToString());
                }
                config.Save(ConfigurationSaveMode.Modified);
            }
        }
        /// <summary>
        /// ����� ��������
        /// </summary>
        public int IntegerValue
        {
            get
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration("");
                if (config.AppSettings.Settings[configName] != null)
                {
                    return int.Parse(config.AppSettings.Settings[configName].Value);
                }
                else
                {
                    config.AppSettings.Settings.Add(configName, DefaultInteger.ToString());
                    config.Save(ConfigurationSaveMode.Modified);
                    return DefaultInteger;
                }
            }
            set
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration("");
                if (config.AppSettings.Settings[configName] != null)
                {
                    config.AppSettings.Settings[configName].Value = value.ToString();
                }
                else
                {
                    config.AppSettings.Settings.Add(configName, value.ToString());
                }
                config.Save(ConfigurationSaveMode.Modified);
            }
        }
    }
}
