﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses
{
    /// <summary>
    /// Интерфейс для решателя вариантов
    /// </summary>
    public interface IMatrixSolver
    {
        /// <summary>
        /// Дополнительные параметры для решения
        /// </summary>
        Parameter[] Parameters { get; }
        /// <summary>
        /// Название метода
        /// </summary>
        String MethodName { get; }
        /// <summary>
        /// Решение (пока в виде строки) если надо будет что-то еще то допишу либо сами
        /// </summary>
        /// <param name="matrix">Матрица с данными</param>
        /// <returns>Строка с решением</returns>
        String Solve(TaskMatrix matrix);
        /// <summary>
        /// Пошаговое решение. Пока что также в виде нескольких строк. Добавил Емеля.
        /// </summary>
        /// <param name="matrix">Матрица с данными</param>
        /// <returns>Строка с полным решением</returns>
        String StepByStepSolution(TaskMatrix matrix);
    }
}
