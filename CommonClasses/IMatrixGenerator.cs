﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses
{
    /// <summary>
    /// Интерфейс для плагинов Генераторов вариантов
    /// </summary>
    public interface IMatrixGenerator
    {
        /// <summary>
        /// Параметры для генератора
        /// </summary>
        Parameter[] Parameters { get;}
        /// <summary>
        /// Метод генерации матрицы
        /// </summary>
        /// <param name="colCount">Число столбцов(свойств)</param>
        /// <param name="rowCount">Число строк(объектов)</param>
        /// <returns>Матрица с данными</returns>
        TaskMatrix Generate(int colCount, int rowCount);
        /// <summary>
        /// Название метода (для отображения в клиенте)
        /// </summary>
        String MethodName { get; }
    }
}
