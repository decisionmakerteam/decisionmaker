﻿using CommonClasses;
using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    class DIM
    {
        //Поля
        public List<int> exist = new List<int>(); // Оставшиеся объекты
        public List<int> rang = new List<int>(); // Объекты с присвоенным рангом
        public double[,] matr; // Матрица показателей
        public double[] weights; // Веса
        public double pow = 0; // Мерность измерения
        public bool[] incDec; // Возрастающий / уменьшающийся рейтинг
        // Свойства
        public double[] maxMin
        {
            get
            {
                double[] res = new double[weights.Length * 2];
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    double max = matr[0, j], min = matr[0, j];
                    for (int i = 1; i < matr.GetLength(0); i++)
                    {
                        double lol = matr[i, j];
                        if (lol < min)
                            min = lol;
                        if (lol > max)
                            max = lol;
                    }
                    if (!incDec[j]) // Если обратный ранг, то мин и макс меняем местами
                    {
                        max += min;
                        min = max - min;
                        max = max - min;
                    }
                    res[j * 2] = max;
                    res[j * 2 + 1] = min;
                }
                return res;
            }
        }
        public double[,] normMatr
        {
            get
            {
                double[,] res = new double[matr.GetLength(0), matr.GetLength(1)];
                double[] boof = maxMin;
                for (int i = 0; i < matr.GetLength(0); i++)
                {
                    for (int j = 0; j < matr.GetLength(1); j++)
                    {
                        res[i, j] = (boof[2 * j] - boof[2 * j + 1] != 0) ?
                            ((double)(matr[i, j] - boof[2 * j + 1])) / (boof[2 * j] - boof[2 * j + 1]) : 1;
                    }
                }
                return res;
            }
        }
        public double[] values
        {
            get
            {
                double[] res = new double[matr.GetLength(0)];
                double[,] boof = normMatr;
                if (pow != 0)
                    for (int i = 0; i < res.Length; i++)
                    {
                        double value = 0;
                        for (int j = 0; j < matr.GetLength(1); j++)
                        {
                            value += Math.Pow(boof[i, j] * weights[j], pow);
                        }
                        value = Math.Pow(value, 1.0 / pow);
                        res[i] = value;
                    }
                else
                {
                    for (int i = 0; i < res.Length; i++)
                    {
                        double value = boof[i, 0] * weights[0];
                        for (int j = 1; j < matr.GetLength(1); j++)
                        {
                            if (value < boof[i, j] * weights[j])
                                value = boof[i, j] * weights[j];
                        }
                        res[i] = value;
                    }
                }
                return res;
            }
        }
        //Конструктор
        public DIM(int n, int m, double power)
        {
            matr = new double[n, m];
            weights = new double[m];
            incDec = new bool[m];
            pow = power;
            for (int i = 0; i < matr.GetLength(0); i++)
                exist.Add(i);
        }
        //Методы
        public string printMaxMin()
        {
            string res = String.Format("{0, 7}", "Max:   ");// "        ";
            double[] boof = maxMin;
            for (int i = 0; i < boof.Length; i += 2)
            {
                res += string.Format("{0, 8}", string.Format("{0,2:0.00}", boof[i]));
            }
            res += "\n" + String.Format("{0, 7}", "Min:   ");
            for (int i = 0; i < boof.Length; i += 2)
            {
                res += string.Format("{0, 8}", string.Format("{0,2:0.00}", boof[i + 1]));
            }
            //res += "\n";
            return res;
        }
        public string printWeights()
        {
            string res = "Wght:   ";
            for (int i = 0; i < weights.Length; i++)
            {
                res += string.Format("{0,6:#0}% ", weights[i] * 100);
            }
            return res;
        }
        public string printMatr()
        {
            string res = "";
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                res += String.Format("Ob{0,2:##}:   ", exist[i] + 1);
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    res += string.Format("{0,7:0.00} ", matr[i, j]);
                }

                res += "\n";
            }
            return res;
        }
        public string printNormMatr()
        {
            string res = "";
            double[,] boof = normMatr;
            for (int i = 0; i < boof.GetLength(0); i++)
            {
                res += String.Format("Ob{0,2:##}:   ", exist[i] + 1);
                for (int j = 0; j < boof.GetLength(1); j++)
                {
                    res += string.Format("{0,7:0.00} ", boof[i, j]);
                }

                res += "\n";
            }
            return res;
        }
        public string printValues()
        {
            string res = "";
            double[] boof = values;
            for (int i = 0; i < boof.Length; i++)
            {
                res += String.Format("Ob{0,2:##}:   ", exist[i] + 1);
                res += string.Format("{0:#0.00} ", boof[i]);
                res += "\n";
            }
            return res;
        }
        public string printRang()
        {
            rang.Reverse();
            string res = "";
            for (int i = 0; i < rang.Count; i++)
            {
                res += String.Format("Ob{0,2:##}:   ", rang[i] + 1);
                res += string.Format("{0:#0.00} ", i + 1);
                res += "\n";
            }
            rang.Reverse();
            return res;
        }
        public string printForIter()
        {
            string res = printWeights();
            res += "\n" + printMaxMin() + String.Format("{0,5}:", "Li") + "\n";
            double[,] boof = normMatr;
            double[] boof2 = values;
            for (int i = 0; i < boof.GetLength(0); i++)
            {
                res += String.Format("Ob{0,2:##}:   ", exist[i] + 1);
                for (int j = 0; j < boof.GetLength(1); j++)
                {
                    res += string.Format("{0,7:0.00} ", boof[i, j]);
                }
                res += string.Format("{0,6:0.00} ", boof2[i]);
                res += "\n";
            }
            return res;
        }
        public void readMatr(string path)
        {
            string[] lines = System.IO.File.ReadAllLines(path);
            string[] str1 = lines[0].Split(' ');
            str1 = lines[1].Split(' ');
            for (int i = 0; i < weights.Length; i++)
            {
                bool boof = incDec[i] = double.Parse(str1[i]) > 0;
                weights[i] = (boof) ? double.Parse(str1[i]) : -double.Parse(str1[i]);
            }
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                str1 = lines[2 + i].Split(' ');
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    matr[i, j] = double.Parse(str1[j]);
                }
            }
        }
        public void fillMatr(CommonClasses.ObjectColumn[] objectColumn, CommonClasses.ObjectRow[] objectRow)
        {
            for (int i = 0; i < objectColumn.Length; i++)
            {
                bool boof = incDec[i] = objectColumn[i].DataType.Equals(DataType.Ascending);
                weights[i] = (boof) ? objectColumn[i].Weight : -objectColumn[i].Weight;
            }
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    matr[i, j] = objectRow[i].Properties[j];
                }
            }
        }
        public void nextStep()
        {
            double[] boofValues = values;
            int pos = 0;
            double min = boofValues[pos];
            if (matr.GetLength(0) > 0)
            {
                for (int i = 1; i < boofValues.Length; i++)
                {
                    if (boofValues[i] < min)
                    {
                        min = boofValues[i];
                        pos = i;
                    }
                }
                rang.Add(exist[pos]);
                exist.RemoveAt(pos);
                deleteLine(pos);
            }
            else
            {
                rang.Add(exist[0]);
                //myDIM.exist.RemoveAt(0);
            }
        }
        internal void cleanMatr()
        {
            List<int> removeLines = new List<int>();
            for (int i = 0; i < this.matr.GetLength(0); i++)
            {
                bool needTOClear = false;
                bool strictlyLess = false;
                for (int j = 0; j < this.matr.GetLength(0); j++)
                {
                    if (i != j)
                    {
                        bool menshe = true;
                        for (int k = 0; k < this.matr.GetLength(1); k++)
                        {
                            if (incDec[k]) // Если увеличивается, то сравниваем какой больше
                            {
                                menshe = menshe && (this.matr[i, k] <= this.matr[j, k]);
                                strictlyLess = strictlyLess || (this.matr[i, k] < this.matr[j, k]);
                            }
                            else // инчае, то сравниваем какой меньше
                            {
                                menshe = menshe && (this.matr[i, k] >= this.matr[j, k]);
                                strictlyLess = strictlyLess || (this.matr[i, k] > this.matr[j, k]);
                            }
                        }
                        needTOClear = needTOClear || menshe && strictlyLess;

                    }
                }
                if (needTOClear)
                    removeLines.Add(i);
            }
            removeLines.Reverse();
            foreach (int i in removeLines)
            {
                this.exist.RemoveAt(i);
                this.deleteLine(i);
            }
        }

        private void deleteLine(int del)
        {
            int shift = 0;
            double[,] res = new double[matr.GetLength(0) - 1, matr.GetLength(1)];
            for (int i = 0; (i < res.GetLength(0)); i++)
            {
                if (i == del)
                    shift++;
                for (int j = 0; j < matr.GetLength(1); j++)
                    res[i, j] = matr[i + shift, j];
            }
            matr = res;
        }

    }
}
