﻿using CommonClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication1;

namespace DisplacedIdealMethod
{
    public class DIMSolveer:IMatrixSolver
    {
        #region IMatrixSolver Members
        /// <summary>
        /// Параметры
        /// </summary>
        public Parameter[] Parameters
        {
            get
            {
                // В данном методе 1 параметр. А именно размерность измеренеия
                Parameter[] res = new Parameter[1];
                res[0] = new Parameter("Мерность измерения",100,0,1,0.5,"Мерность");
                return res;
            }
        }
        /// <summary>
        /// Название метода
        /// </summary>
        public string MethodName
        {
            get { return "Метод смещенного идеала"; }
        }
        /// <summary>
        /// Решение матрицы
        /// </summary>
        /// <param name="matrix">Матрица</param>
        /// <returns>Решение</returns>
        public string Solve(TaskMatrix matrix)
        {
            string res = "";
            DIM myDIM = new DIM(matrix.Rows.Length, matrix.Columns.Length, Parameters[0].DoubleValue);
            myDIM.fillMatr(matrix.Columns, matrix.Rows);
            myDIM.cleanMatr();
            while (myDIM.matr.GetLength(0) > 0)
                myDIM.nextStep();
            res += myDIM.printRang();
            return res;
        }

        public string StepByStepSolution(TaskMatrix matrix)
        {
            int iter = 1;
            string res = "";
            DIM myDIM = new DIM(matrix.Rows.Length, matrix.Columns.Length, Parameters[0].DoubleValue);
            myDIM.fillMatr(matrix.Columns, matrix.Rows);
            res += "Info:" + "/n";
            res += myDIM.printWeights() + "/n";
            res += myDIM.printMatr() + "/n";
            res += "Iteration 0 (clean up):" + "/n";
            myDIM.cleanMatr();
            res += myDIM.printWeights() + "/n";
            res += myDIM.printMatr() + "/n";

            while (myDIM.matr.GetLength(0) > 0)
            {
                res += string.Format("Iteration {0}:", iter);
                res += myDIM.printForIter() + "/n";
                iter++;
                res += "\n";
                myDIM.nextStep();
            }
            res += myDIM.printRang();
            return res;
        }
        
        #endregion
    }
}
