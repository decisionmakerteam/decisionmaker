﻿using CommonClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainClient
{
    /// <summary>
    /// Основная программа
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Плагина - генераторы вариантов
        /// </summary>
        private List<IMatrixGenerator> generators;
        /// <summary>
        /// Плагины - решатели вариантов
        /// </summary>
        private List<IMatrixSolver> solvers;
        /// <summary>
        /// Матрица с данными
        /// </summary>
        TaskMatrix matrix;
        /// <summary>
        /// Конструктор
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Решение матрицы плагином
        /// </summary>
        /// <param name="sender">Кнопка в меню</param>
        /// <param name="e">Параметры</param>
        private void SolveMatrix(object sender, EventArgs e)
        {
            ToolStripDropDownItem control = sender as ToolStripDropDownItem;
            IMatrixSolver sol = solvers[(int)control.Tag];
            MessageBox.Show(sol.Solve(matrix));
        }
        /// <summary>
        /// Генерация матрицы плагином
        /// </summary>
        /// <param name="sender">Кнопка в меню</param>
        /// <param name="e">Параметры</param>
        private void GenerateMatrix(object sender, EventArgs e)
        {
            ToolStripDropDownItem control = sender as ToolStripDropDownItem;
            IMatrixGenerator gen = generators[(int)control.Tag];
            matrix = gen.Generate((int)colInput.Value, (int)rowInput.Value);
            if (matrix != null)
            {
                dataTable.DataSource = matrix.ToDataTable();
                for (int i = 0; i < solveToolStripMenuItem.DropDownItems.Count; i++)
                    solveToolStripMenuItem.DropDownItems[i].Enabled = true;
            }
        }
        /// <summary>
        /// Настройки для плагина генератора
        /// </summary>
        /// <param name="sender">Кнопка в меню</param>
        /// <param name="e">Параметры</param>
        private void GenerateSettings(object sender, EventArgs e)
        {
            ToolStripDropDownItem control = sender as ToolStripDropDownItem;
            IMatrixGenerator gen = generators[(int)control.Tag];
            SettingsForm sf = new SettingsForm();
            sf.ShowDialog(gen.Parameters, gen.MethodName);
        }
        /// <summary>
        /// Настройки для плагина решателя
        /// </summary>
        /// <param name="sender">Кнопка в меню</param>
        /// <param name="e">Параметры</param>
        private void SolveSettings(object sender, EventArgs e)
        {
            ToolStripDropDownItem control = sender as ToolStripDropDownItem;
            IMatrixSolver sol = solvers[(int)control.Tag];
            SettingsForm sf = new SettingsForm();
            sf.ShowDialog(sol.Parameters, sol.MethodName);
        }
        /// <summary>
        /// Загрузить плагины - решатели
        /// </summary>
        private void LoadSolverPlugins()
        {
            string path = Application.StartupPath;
            string[] pluginFiles = Directory.GetFiles(path + "\\SolverPlugins", "*.DLL");
            solvers = new List<IMatrixSolver>();
            for (int i = 0; i < pluginFiles.Length; i++)
            {
                try
                {
                    Assembly assembly = Assembly.LoadFile(pluginFiles[i]);
                    foreach (Type type in assembly.GetTypes())
                    {
                        bool match = false;
                        foreach (Type iface in type.GetInterfaces())
                        {
                            if (iface.FullName == typeof(IMatrixSolver).FullName)
                                match = true;
                        }
                        if (match)
                        {
                            IMatrixSolver temp = (IMatrixSolver)assembly.CreateInstance(type.FullName);
                            solvers.Add(temp);
                            solveToolStripMenuItem.DropDownItems.Add(temp.MethodName);
                            solveToolStripMenuItem.DropDownItems[solvers.Count - 1].Tag = solvers.Count - 1;
                            solveToolStripMenuItem.DropDownItems[solvers.Count - 1].Click += SolveMatrix;
                            solveToolStripMenuItem.DropDownItems[solvers.Count - 1].Enabled = false;
                            solveSettingsToolStripMenuItem.DropDownItems.Add(temp.MethodName);
                            solveSettingsToolStripMenuItem.DropDownItems[solvers.Count - 1].Tag = solvers.Count - 1;
                            solveSettingsToolStripMenuItem.DropDownItems[solvers.Count - 1].Click += SolveSettings;
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Не удалось загрузить плагины, читающие треки");
                }
            }
        }
        /// <summary>
        /// Загрузить плагины - генераторы
        /// </summary>
        private void LoadGeneratorPlugins()
        {
            string path = Application.StartupPath;
            string[] pluginFiles = Directory.GetFiles(path + "\\GeneratorPlugins", "*.DLL");
            generators = new List<IMatrixGenerator>();
            for (int i = 0; i < pluginFiles.Length; i++)
            {
                try
                {
                    Assembly assembly = Assembly.LoadFile(pluginFiles[i]);
                    foreach (Type type in assembly.GetTypes())
                    {
                        bool match = false;
                        foreach (Type iface in type.GetInterfaces())
                        {
                            if (iface.FullName == typeof(IMatrixGenerator).FullName)
                                match = true;
                        }
                        if (match)
                        {
                            IMatrixGenerator temp = (IMatrixGenerator)assembly.CreateInstance(type.FullName);
                            generators.Add(temp);
                            generateToolStripMenuItem.DropDownItems.Add(temp.MethodName);
                            generateToolStripMenuItem.DropDownItems[generators.Count - 1].Tag = generators.Count - 1;
                            generateToolStripMenuItem.DropDownItems[generators.Count - 1].Click += GenerateMatrix;
                            genSettingsToolStripMenuItem.DropDownItems.Add(temp.MethodName);
                            genSettingsToolStripMenuItem.DropDownItems[generators.Count - 1].Tag = generators.Count - 1;
                            genSettingsToolStripMenuItem.DropDownItems[generators.Count - 1].Click += GenerateSettings;
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Не удалось загрузить плагины, читающие треки");
                }
            }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="sender">Форма</param>
        /// <param name="e">Параметры</param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadSolverPlugins();
            LoadGeneratorPlugins();
        }

        private void toExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (matrix!=null && saveToExcel.ShowDialog() == DialogResult.OK)
            {
                XlsWriter.WriteExcelFile(saveToExcel.FileName, matrix.ToDataTable());
            }
        }
    }
}
