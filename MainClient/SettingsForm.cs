using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using CommonClasses;

namespace MainClient
{
    public partial class SettingsForm : Form
    {

        private int startX = 10;
        private int startY = 10;
        private const int WIDTH = 200;
        private List<Control> inputs;
        private Parameter[] parameters;
        public SettingsForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowDialog(Parameter[] parames, String mName)
        {
            Text = mName;
            inputs = new List<Control>();
            int curX = startX;
            int curY = startY;
            this.parameters = parames;
            foreach (Parameter parameter in this.parameters)
            {
                Label newLabel = new Label();
                newLabel.Text = parameter.Name;
                newLabel.Location = new Point(curX, curY);
                newLabel.Width = WIDTH;
                Controls.Add(newLabel);
                if (parameter.ParameterType==ParameterType.Boolean)
                {
                    CheckBox newCb = new CheckBox();
                    newCb.Location = new Point(curX + WIDTH, curY);
                    newCb.Checked = parameter.BooleanValue;
                    Controls.Add(newCb);
                    inputs.Add(newCb);
                    newCb.Tag = parameter.Name;
                }
                else if (parameter.ParameterType==ParameterType.Double)
                {
                    NumericUpDown newNud = new NumericUpDown();
                    newNud.Location = new Point(curX + WIDTH, curY);
                    newNud.Value = (decimal)parameter.DoubleValue;
                    newNud.Maximum = (decimal)parameter.MaximumDouble;
                    newNud.Minimum = (decimal)parameter.MinimumDouble;
                    newNud.ThousandsSeparator = true;
                    int steps=(int)-Math.Log(parameter.Step, 10)+1;
                    if (steps<0)
                    {
                        steps = 0;
                    }
                    newNud.DecimalPlaces = steps;
                    newNud.Increment = (decimal)parameter.Step;
                    Controls.Add(newNud);
                    inputs.Add(newNud);
                    newNud.Tag = parameter.Name;
                }
                else
                {
                    NumericUpDown newNud = new NumericUpDown();
                    newNud.Location = new Point(curX + WIDTH, curY);
                    newNud.Value = parameter.IntegerValue;
                    newNud.Maximum = parameter.MaximumIneteger;
                    newNud.Minimum = parameter.MinimumIneteger;
                    newNud.Increment = 1;
                    Controls.Add(newNud);
                    inputs.Add(newNud);
                    newNud.Tag = parameter.Name;
                }
                curY += 20;
            }
            return ShowDialog();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < parameters.Length; i++)
            {
                if (parameters[i].ParameterType == ParameterType.Boolean)
                {
                    CheckBox cb = inputs[i] as CheckBox;
                    parameters[i].BooleanValue = cb.Checked;
                }
                else if (parameters[i].ParameterType==ParameterType.Integer)
                {
                    NumericUpDown nud = inputs[i] as NumericUpDown;
                    parameters[i].IntegerValue = (int)nud.Value;
                }
                else
                {
                    NumericUpDown nud = inputs[i] as NumericUpDown;
                    parameters[i].DoubleValue = (double)nud.Value;
                }
            }
        }

    }
}