﻿namespace MainClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataTable = new System.Windows.Forms.DataGridView();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.fileMenu = new System.Windows.Forms.MenuStrip();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.genSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solveSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colInput = new System.Windows.Forms.NumericUpDown();
            this.rowInput = new System.Windows.Forms.NumericUpDown();
            this.saveToExcel = new System.Windows.Forms.SaveFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            this.fileMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowInput)).BeginInit();
            this.SuspendLayout();
            // 
            // dataTable
            // 
            this.dataTable.AllowUserToAddRows = false;
            this.dataTable.AllowUserToDeleteRows = false;
            this.dataTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataTable.Location = new System.Drawing.Point(0, 92);
            this.dataTable.Margin = new System.Windows.Forms.Padding(2);
            this.dataTable.Name = "dataTable";
            this.dataTable.ReadOnly = true;
            this.dataTable.RowTemplate.Height = 24;
            this.dataTable.Size = new System.Drawing.Size(535, 288);
            this.dataTable.TabIndex = 0;
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 380);
            this.statusBar.Name = "statusBar";
            this.statusBar.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
            this.statusBar.Size = new System.Drawing.Size(535, 22);
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.generateToolStripMenuItem,
            this.solveToolStripMenuItem,
            this.genSettingsToolStripMenuItem,
            this.solveSettingsToolStripMenuItem});
            this.fileMenu.Location = new System.Drawing.Point(0, 0);
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.fileMenu.Size = new System.Drawing.Size(535, 24);
            this.fileMenu.TabIndex = 2;
            this.fileMenu.Text = "menuStrip1";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toExcelToolStripMenuItem});
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.saveToolStripMenuItem.Text = "Сохранить";
            // 
            // toExcelToolStripMenuItem
            // 
            this.toExcelToolStripMenuItem.Name = "toExcelToolStripMenuItem";
            this.toExcelToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.toExcelToolStripMenuItem.Text = "toExcel";
            this.toExcelToolStripMenuItem.Click += new System.EventHandler(this.toExcelToolStripMenuItem_Click);
            // 
            // generateToolStripMenuItem
            // 
            this.generateToolStripMenuItem.Name = "generateToolStripMenuItem";
            this.generateToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.generateToolStripMenuItem.Text = "Генераторы";
            // 
            // solveToolStripMenuItem
            // 
            this.solveToolStripMenuItem.Name = "solveToolStripMenuItem";
            this.solveToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.solveToolStripMenuItem.Text = "Решатели";
            // 
            // genSettingsToolStripMenuItem
            // 
            this.genSettingsToolStripMenuItem.Name = "genSettingsToolStripMenuItem";
            this.genSettingsToolStripMenuItem.Size = new System.Drawing.Size(151, 20);
            this.genSettingsToolStripMenuItem.Text = "Настройки генераторов";
            // 
            // solveSettingsToolStripMenuItem
            // 
            this.solveSettingsToolStripMenuItem.Name = "solveSettingsToolStripMenuItem";
            this.solveSettingsToolStripMenuItem.Size = new System.Drawing.Size(143, 20);
            this.solveSettingsToolStripMenuItem.Text = "Настройки решателей";
            // 
            // colInput
            // 
            this.colInput.Location = new System.Drawing.Point(143, 33);
            this.colInput.Margin = new System.Windows.Forms.Padding(2);
            this.colInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.colInput.Name = "colInput";
            this.colInput.Size = new System.Drawing.Size(90, 20);
            this.colInput.TabIndex = 3;
            this.colInput.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // rowInput
            // 
            this.rowInput.Location = new System.Drawing.Point(143, 57);
            this.rowInput.Margin = new System.Windows.Forms.Padding(2);
            this.rowInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.rowInput.Name = "rowInput";
            this.rowInput.Size = new System.Drawing.Size(90, 20);
            this.rowInput.TabIndex = 4;
            this.rowInput.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // saveToExcel
            // 
            this.saveToExcel.DefaultExt = "xls";
            this.saveToExcel.FileName = "book";
            this.saveToExcel.Filter = "Excel files (*.xlsx)|*.xlsx";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Количество критериев:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Количество объектов";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 402);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rowInput);
            this.Controls.Add(this.colInput);
            this.Controls.Add(this.fileMenu);
            this.Controls.Add(this.dataTable);
            this.Controls.Add(this.statusBar);
            this.MainMenuStrip = this.fileMenu;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Text = "Теория принятия решений";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            this.fileMenu.ResumeLayout(false);
            this.fileMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataTable;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.MenuStrip fileMenu;
        private System.Windows.Forms.ToolStripMenuItem generateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem genSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solveSettingsToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown colInput;
        private System.Windows.Forms.NumericUpDown rowInput;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toExcelToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveToExcel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

