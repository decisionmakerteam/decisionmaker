﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainClient
{
    public static class XlsWriter
    {
        /// <summary>
        /// Строка для подключения к Excel
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <returns>Строка подключения</returns>
        private static string GetConnectionString(String fileName)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            // XLSX - Excel 2007, 2010, 2012, 2013
            props["Provider"] = "Microsoft.ACE.OLEDB.12.0;";
            props["Extended Properties"] = "Excel 12.0 XML";
            props["Data Source"] = fileName;

            // XLS - Excel 2003 and Older
            //props["Provider"] = "Microsoft.Jet.OLEDB.4.0";
            //props["Extended Properties"] = "Excel 8.0";
            //props["Data Source"] = fileName;

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> prop in props)
            {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }

            return sb.ToString();
        }
        /// <summary>
        /// Записать в файо Excel
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <param name="dataTable">Таблица с данными</param>
        public static void WriteExcelFile(String fileName, DataTable dataTable)
        {
            string connectionString = GetConnectionString(fileName);
            if (File.Exists(fileName))
                File.Delete(fileName);
            List<String> correctNames = new List<string>();
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                String colName = dataTable.Columns[i].ColumnName.Replace("\r\n", "").Replace(".","");
                int count = correctNames.Count(x => x.Contains(colName));
                if (count == 0)
                    correctNames.Add(colName);
                else
                    correctNames.Add(String.Format("{0} {1}", colName, count + 1));
            }
            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = conn;
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE TABLE ["+dataTable.TableName.Replace(" ","_")+"] (");
                for (int i = 0; i < dataTable.Columns.Count; i++)
                    sb.AppendFormat("[{0}] TEXT, ", correctNames[i]);
                sb.Length -= 2;
                sb.Append(");");
                cmd.CommandText = sb.ToString();
                cmd.ExecuteNonQuery();
                foreach (DataRow row in dataTable.Rows)
                {
                    sb = new StringBuilder();
                    sb.Append("INSERT INTO [" + dataTable.TableName.Replace(" ", "_") + "](");
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                        sb.AppendFormat("[{0}], ", correctNames[i]);
                    sb.Length -= 2;
                    sb.Append(") VALUES(");
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                        sb.AppendFormat("'{0}', ", row[i]);
                    sb.Length -= 2;
                    sb.Append(");");
                    cmd.CommandText = sb.ToString();
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }
    }
}
