﻿using CommonClasses;
using System;
using System.Collections.Generic;

namespace DisplaceIdealGenerator
{

    /// <summary>
    /// Генератор 
    /// </summary>
    public class DisplaceIdealGenerator : IMatrixGenerator
    {
        /// <summary>
        /// Параметры
        /// </summary>
        private Parameter[] parameters = null;

        #region IMatrixGenerator Members
        /// <summary>
        /// Параметры
        /// </summary>
        public Parameter[] Parameters
        {
            get
            {
                if(parameters == null || parameters.Length != 3 + 4 * parameters[0].IntegerValue)
                {
                    int criteriaCount = parameters == null ? 3 : parameters[0].IntegerValue;
                    parameters = new Parameter[3 + 4 * criteriaCount];
                    parameters[0] = new Parameter("Количество критериев", 5, 1, 3, "gen-criteria-count");
                    parameters[1] = new Parameter("Количество объектов", 100, 1, 6, "gen-object-count");
                    parameters[2] = new Parameter("Количество объектов, отсеиваемых по Парето-оптимальности", 
                        100, 0, 2, "gen-object-pareto");
                    for(int i = 3; i < 3 + 4 * parameters[0].IntegerValue; i += 4)
                    {
                        parameters[i] = new Parameter("Максимальное значение " + i / 4 + " критерия", 1000, 2, 20, 
                            "gen-criterion" + i / 4 + "-maxvalue");
                        parameters[i + 1] = new Parameter("Минимальное значение " + i / 4 + " критерия", 900, 0, 0, 
                            "gen-criterion" + i / 4 + "-minvalue");
                        parameters[i + 2] = new Parameter("Вес " + i / 4 + " критерия", 1.0, 0.0, 1.0, 0.5, 
                            "gen-criterion" + i / 4 + "-weight");
                        parameters[i + 3] = new Parameter("Возрастающий критерий " + i / 4, true, 
                            "gen-criterion" + i / 4 + "-ascending");
                    }
                }
                return parameters;
            }
        }
        
                
        /// <summary>
        /// Генерация матрицы - аргументы не используются, для задания параметров генерации используется Parameters
        /// </summary>
        /// <param name="colCount">Число столбцов</param>
        /// <param name="rowCount">Число строк</param>
        /// <returns>Матрица</returns>
        public TaskMatrix Generate(int colCount, int rowCount)
        {
            RefreshCriteria();
            TaskMatrix res;
            if (CanGenerate())
            {
                res = new TaskMatrix(MethodName, getCriteriaCount());
                createColumns(res, getCriteriaCount());
                createRows(res, getObjectCount());

                int paretoMax = GeneratePareto(res);
                generatedCount = getParetoCount();

                GenerateCap(paretoMax, res);
                generatedCount += 2;
                
                GenerateDistinct(paretoMax, res);
                GenerateOther(paretoMax, res);
                generatedCount = getObjectCount();
                ShiftObjects(res);
                MixObjects(res);
            }
            else
            {
                res = null;
            }
            return res;
        }

        
        /// <summary>
        /// Название метода
        /// </summary>
        public string MethodName
        {
            get { return "Метод смещённого идеала"; }
        }

        #endregion

        private int getCriteriaCount()
        {
            return Parameters[0].IntegerValue;
        }

        private int getObjectCount()
        {
            return Parameters[1].IntegerValue;
        }

        private int getParetoCount()
        {
            return Parameters[2].IntegerValue;
        }
        private int getCriterionMax(int criterion)
        {
            return Parameters[3 + 4 * criterion].IntegerValue;
        }

        private int getCriterionMin(int criterion)
        {
            return Parameters[4 + 4 * criterion].IntegerValue;
        }

        private int getCriterionWeight(int criterion)
        {
            return Parameters[5 + 4 * criterion].IntegerValue;
        }

        private bool isCriterionAscending(int criterion)
        {
            return Parameters[6 + 4 * criterion].BooleanValue;
        }

        private Random random = new Random();

        private int[] ranges;

        private int[] criteria;

        private List<int> chosenCriteria = new List<int>();

        private List<long> hashes = new List<long>();

        private int generatedCount;

        /// <summary>
        /// Обновляет массивы критериев и диапазонов критериев
        /// </summary>
        private void RefreshCriteria()
        {
            criteria = new int[getCriteriaCount()];
            ranges = new int[getCriteriaCount()];
            for (int i = 0; i < criteria.Length; i++)
                criteria[i] = i;
            for (int i = 0; i < criteria.Length; i++)
            {

                //Включая и минимальное, и максимальное значения критериев

                ranges[i] = getCriterionMax(i) - getCriterionMin(i) + 1;
            }
            SortCriteriaByRangeDesc();
        }

        /// <summary>
        /// Определяет, можно ли сгенерировать задачу по таким входным данным
        /// </summary>
        /// <returns>TRUE, если можно, FALSE иначе</returns>
        private bool CanGenerate()
        {
            // +1 в аргументе getPointCount() за счёт "заглушки"
            return getParetoCount() + getPointCount(getParetoCount() + 1) >= getObjectCount() - 2;
        }

        /// <summary>
        /// Получает количество возможных объектов, 
        /// где значения каждого критерия каждого объекта больше или равно аргументу 
        /// </summary>
        /// <param name="bottom">Аргумент</param>
        /// <returns>Количество объектов</returns>
        private int getPointCount(int bottom)
        {
            int result = 1;
            for (int i = 0; i < criteria.Length; i++)
            {
                if (bottom >= ranges[i])
                    return 0;
                result *= (ranges[i] - bottom);
            }
            return result;
        }

        private int getChosenPointCount(int bottom)
        {
            int result = 1;
            for (int i = 0; i < chosenCriteria.Count; i++)
            {
                if (bottom >= ranges[chosenCriteria[i]])
                    return 0;
                result *= (ranges[chosenCriteria[i]] - bottom);
            }
            return result;
        }

        /// <summary>
        /// Создание критериев - пока что все критерии имеют один и тот же вес и возрастают
        /// </summary>
        /// <param name="res">Генерируемая матрица</param>
        /// <param name="colCount">Число критериев (столбцов)</param>
        private void createColumns(TaskMatrix matrix, int colCount)
        {
            for(int i = 0; i < colCount; i++)
                matrix.AddColumn("Column " + i, 1, DataType.Ascending);
        }

        private void createRows(TaskMatrix matrix, int rowCount)
        {
            List<double> list;
            for (int i = 0; i < rowCount; i++) {
                list = new List<double>();
                for (int j = 0; j < matrix.Columns.Length; j++)
                    list.Add(0);
                matrix.AddRow(new ObjectRow("Object " + i, list));
            }
        }

        /// <summary>
        /// Сгенерировать объекты, отсеиваемые по Парето-оптимальности
        /// </summary>
        /// <param name="matrix">Генерируемая матрица</param>
        private int GeneratePareto(TaskMatrix matrix)
        {
            int paretoMax = getParetoMax();
            int top;
            int bottom = 0;
            int max = 0;
            int rand = 0;
            for (int i = 0; i < getParetoCount(); i++)
            {
                top = bottom + (paretoMax - bottom) / (getParetoCount() - i);
                for (int j = 0; j < criteria.Length; j++)
                {
                    rand = random.Next(bottom, top + 1);//ВКЛЮЧАЯ top
                    matrix.Rows[i].setProperty(j, rand);
                    if (rand > max)
                        max = rand;
                }
                bottom = max + 1;
            }
            return max;
        }

        private int getParetoMax()
        {
            int bottom = getParetoCount();
            int top = ranges[0];
            for (int i = 0; i < criteria.Length; i++)
            {
                if (top > ranges[i])
                    top = ranges[i];
            }
            while (getPointCount(top + 1) < getObjectCount() - getParetoCount() - 2) 
            {
                top = random.Next(bottom, top);//ИСКЛЮЧАЯ top
            }
            return top;
        }

        private void GenerateCap(int paretoMax, TaskMatrix result)
        {
            int criterion1 = random.Next(criteria.Length);
            int criterion2 = (criterion1 + 1 + random.Next(criteria.Length - 1)) % criteria.Length;

            result.Rows[getParetoCount()].setProperty(criterion1, paretoMax);
            result.Rows[getParetoCount() + 1].setProperty(criterion2, paretoMax);

            for (int i = (criterion1 + 1) % criteria.Length; i != criterion1; i = (i + 1)
                    % criteria.Length)
            {
                result.Rows[getParetoCount()].setProperty(i, random.Next(paretoMax + 1, ranges[i]));
            }
            for (int i = (criterion2 + 1) % criteria.Length; i != criterion2; i = (i + 1)
                    % criteria.Length)
            {
                result.Rows[getParetoCount() + 1].setProperty(i, random.Next(paretoMax + 1, ranges[i]));
            }
        }

        private void GenerateDistinct(int paretoMax, TaskMatrix result)
        {
            chosenCriteria.Clear();
            int objCount = getObjectCount() - generatedCount;
            if (ranges[criteria[0]] > objCount)
            {
                chosenCriteria.Add(criteria[0]);
                int top, bottom = paretoMax + 1;
                for (int i = generatedCount; i < getObjectCount(); i++)
                {
                    top = bottom + (ranges[criteria[0]] - bottom)
                            / (objCount - i + generatedCount) - 1;
                    result.Rows[i].setProperty(criteria[0], random.Next(bottom, top + 1));
                    bottom = (int) result.Rows[i].Properties[criteria[0]] + 1;
                }
            }
            else {
                for (int i = 0, pointCount = 1; pointCount < objCount; i++, pointCount *= ranges[criteria[i]]
                        - paretoMax)
                {
                    chosenCriteria.Add(criteria[i]);
                }
                FillHashes(paretoMax);
                int rand, digitCount;
                long buf;
                for (int i = generatedCount; i < getObjectCount(); i++)
                {
                    rand = random.Next(hashes.Count - i - generatedCount);
                    buf = hashes[rand];
                    for (int j = 0; j < chosenCriteria.Count; j++)
                    {
                        digitCount = getDigitCount(j, paretoMax);
                        result.Rows[i].setProperty(chosenCriteria[j], (int)(buf % digitCount));
                        hashes[rand] = buf / digitCount;
                    }
                    hashes[rand] = hashes[hashes.Count - i - 1 + generatedCount];
                    hashes[hashes.Count - 1 - i + generatedCount] = buf;
                }
            }
        }

        private void FillHashes(int bottom)
        {
            hashes.Clear();
            int[] current = new int[chosenCriteria.Count];
            for (int i = 0; i < current.Length; i++)
            {
                current[i] = bottom + 1;
            }
            int pointCount = getChosenPointCount(bottom);
            for (int count = 0; count < pointCount; count++)
            {
                hashes.Add(getHash(current, bottom));
                for (int i = 0; i < current.Length; i++)
                {
                    if (current[i] < ranges[chosenCriteria[i]])
                    {
                        current[i]++;
                        for (int j = 0; j < i; j++)
                            current[j] = bottom;
                        break;
                    }
                }
            }
        }

        private long getHash(int[] obj, int bottom)
        {
            long result = 0;
            int digitCount = 0;
            for (int i = 0; i < obj.Length; i++)
            {
                result += obj[i] * (long)Math.Pow(10, digitCount);
                digitCount += getDigitCount(i, bottom);
            }
            return result;
        }

        private int getDigitCount(int criterion, int bottom)
        {
            int result = 0;
            int range = ranges[chosenCriteria[criterion]] - bottom;
            while (range > 0)
            {
                result++;
                range /= 10;
            }
            return result;
        }

        private void GenerateOther(int paretoMax, TaskMatrix result)
        {
            for (int i = generatedCount; i < getObjectCount(); i++)
            {
                for (int j = 0; j < criteria.Length; j++)
                {
                    if (chosenCriteria.IndexOf(j) == -1)
                    {
                        result.Rows[i].setProperty(j, random.Next(paretoMax + 1, ranges[j]));
                    }
                }
            }
        }

        private void ShiftObjects(TaskMatrix objects)
        {
            for(int i = 0; i < criteria.Length; i++)
            {
                if (!isCriterionAscending(i))
                {
                    for(int j = 0; j < getObjectCount(); j++)
                    {
                        objects.Rows[j].setProperty(i, ranges[i] - objects.Rows[j].Properties[i] - 1);
                    }
                }
            }
            for (int i = 0; i < getObjectCount(); i++)
            {
                for (int j = 0; j < criteria.Length; j++)
                {
                    objects.Rows[i].setProperty(j, objects.Rows[i].Properties[j] + getCriterionMin(j));
                }
            }
        }

        private void MixObjects(TaskMatrix objects)
        {
            ObjectRow buf;
            int rand;
            for(int i = 0; i < objects.Rows.Length; i++)
            {
                rand = random.Next(objects.Rows.Length);
                buf = objects.Rows[i];
                objects.Rows[i] = objects.Rows[rand];
                objects.Rows[rand] = buf;
            }
        }
        /// <summary>
        /// Сортировка критериев по убыванию диапазона значений
        /// </summary>
        private void SortCriteriaByRangeDesc()
        {
            int buf;
            for (int i = 0; i < criteria.Length; i++)
                for (int j = i; j > 0
                        && ranges[criteria[j]] > ranges[criteria[j - 1]]; j--)
                {
                    buf = criteria[j];
                    criteria[j] = criteria[j - 1];
                    criteria[j - 1] = buf;
                }
        }
    }
}
