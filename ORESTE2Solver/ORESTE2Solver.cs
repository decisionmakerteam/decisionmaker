﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonClasses;

namespace ORESTE2Solver
{
    public class ORESTE2Solver:IMatrixSolver
    {
        private Parameter[] parameters = null;
        public Parameter[] Parameters
        {
            get
            {
                if (parameters == null)
                {
                    parameters = new Parameter[3];
                    parameters[1] = new Parameter("b(betta)", Int32.MaxValue, 0, 0.5, 0.0001, "oreste2-b");
                    parameters[2] = new Parameter("g(gamma)", Int32.MaxValue, 0, 0.5, 0.0001, "oreste2-g");
                }
                parameters[0] = new Parameter("a(alpha)", (double)(parameters[1].DoubleValue * (1.0 + parameters[2].DoubleValue)), (double)(parameters[1].DoubleValue * (1.0 + parameters[2].DoubleValue)), parameters[1].DoubleValue * (1.0 + parameters[2].DoubleValue), 0.0001, "oreste2-a");
                return parameters;
            }
        }

        public string MethodName
        {
            get
            {
                return "ORESTE II";
            }
        }

        public string Solve(TaskMatrix matrix)
        {
            double[,] matr;
            List<int> deletedRows;
            List<String> names;
            List<double> summs = SolveOresto1(matrix, out matr, out deletedRows, out names);
            double[,] c = new double[matr.GetLength(0)-1, matr.GetLength(0)-1];
            for(int i=0; i<matr.GetLength(0)-1; ++i)
            {
                for(int l=0; l<matr.GetLength(0)-1; ++l)
                {
                    double sum = 0;
                    for (int j = 0; j < matrix.Columns.Length; ++j)
                        sum += matr[l + 1, j] - matr[i + 1, l] + Math.Abs(matr[l + 1, j] - matr[i + 1, l]);
                    c[i, l] = (0.5 * sum)/(Math.Pow(matrix.Columns.Length, 2)*(matr.GetLength(0)-1-1));
                }
            }
            char[,] syms = new char[matr.GetLength(0) - 1, matr.GetLength(0) - 1];
            double b = Parameters[1].DoubleValue;
            double g = Parameters[2].DoubleValue;
            double a = b * (1 + g);
            Parameters[0] = new Parameter("a(alpha)", a,a,a,0, "oreste2-a");
            StringBuilder res = new StringBuilder();
            int leftOuterIndent = Convert.ToInt32(matrix.Rows.Max(x => x.Name.Length)*1.5);
            String text = "{0," + leftOuterIndent.ToString() + "}";
            res.Append(' ', leftOuterIndent);
            for (int i = 0; i < matrix.Rows.Length; i++)
                if(!deletedRows.Contains(i))
                {
                    res.AppendFormat(text, matrix.Rows[i].Name);
                }
            res.AppendLine();

            for(int i=0; i<c.GetLength(0); ++i)
            {
                for(int j=i; j<c.GetLength(0); ++j)
                {
                    if ((i==j) || ((Math.Abs(c[i, j] - c[j, i]) <= b) && (c[i, j]<=a) && (c[j, i]<=a)))
                    {
                        syms[i, j] = '~';
                        syms[j, i] = '~';
                    }
                    else if ((Math.Abs(c[i, j] - c[j, i]) > b) && (c[j, i]/(Math.Abs(c[i, j]-c[j, i])) <= g))
                    {
                        syms[i, j] = '>';
                        syms[j, i] = '<';
                    }
                    else
                    {
                        syms[i, j] = 'N';
                        syms[j, i] = 'N';
                    }
                }
            }
            for (int i = 0; i < c.GetLength(0); ++i)
            {
                res.AppendFormat(text, names[i]);
                for (int j = 0; j < c.GetLength(0); ++j)
                    res.AppendFormat(text, syms[i, j]);
                res.AppendLine();
            }
            return res.ToString();
        }

        private List<double> SolveOresto1(TaskMatrix matrix, out double[,] matr, out List<int> deletingRows, out List<String> names)
        {
            double a = Parameters[0].DoubleValue;
            //Заполняем матрицу
            matr = new double[matrix.Rows.Length + 1, matrix.Columns.Length];
            for (int i = 0; i < matrix.Columns.Length; ++i)
                matr[0, i] = matrix.Columns[i].Weight;
            for (int i = 0; i < matrix.Rows.Length; i++)
                for (int j = 0; j < matrix.Columns.Length; j++)
                    matr[i + 1, j] = matrix.Rows[i].Properties[j];
            //Делаем выборку объектов оптимальной по Порето
            deletingRows = new List<int>();
            for (int i = 1; i < matr.GetLength(0); i++)
            {
                for (int j = 1; j < matr.GetLength(0); j++)
                {
                    if (i != j)
                    {
                        bool unneed = true;
                        for (int k = 0; k < matrix.Columns.Length; k++)
                        {
                            bool dominator = matr[i, k] >= matr[j, k];
                            if (matrix.Columns[k].DataType == DataType.Descending)
                                dominator = !dominator;
                            unneed = unneed && dominator;
                        }
                        if (unneed && !deletingRows.Contains(i - 1) && !deletingRows.Contains(j - 1))
                            deletingRows.Add(j - 1);
                    }
                }
            }
            //Убираем ненужные строки
            names = new List<String>();
            double[,] nmatr = new double[matrix.Rows.Length + 1 - deletingRows.Count, matrix.Columns.Length];
            for (int k = 0; k < matrix.Columns.Length; k++)
                nmatr[0, k] = matr[0, k];
            int l = 0;
            for (int i = 1; i < matr.GetLength(0); i++)
            {
                if (!deletingRows.Contains(i-1))
                {
                    l += 1;
                    for (int k = 0; k < matrix.Columns.Length; k++)
                        nmatr[l, k] = matr[i, k];
                    names.Add(matrix.Rows[i-1].Name);
                }
            }
            matr = nmatr;
            //Ранжируем критерии
            List<double> criterias = new List<double>();
            for (int i = 0; i < matrix.Columns.Length; ++i)
                criterias.Add(matr[0, i]);
            var replaceCriterias = getReplacementRank(criterias);
            for (int i = 0; i < matrix.Columns.Length; ++i)
                matr[0, i] = replaceCriterias[matr[0, i]];
            //Ранжируем столбцы показателей
            for (int j = 0; j < matrix.Columns.Length; ++j)
            {
                List<double> values = new List<double>();
                for (int i = 0; i < matr.GetLength(0)-1; ++i)
                    values.Add(matr[i + 1, j]);
                var replacedValues = getReplacementRank(values, matrix.Columns[j].DataType);
                for (int i = 0; i < matr.GetLength(0)-1; ++i)
                    matr[i + 1, j] = replacedValues[matr[i + 1, j]];
            }
            //Вычисляем проекции
            for (int i = 0; i < matr.GetLength(0)-1; ++i)
                for (int j = 0; j < matrix.Columns.Length; ++j)
                    matr[i + 1, j] = (1 - a) * matr[i + 1, j] + a * matr[0, j];
            //Ранжируем все показатели
            List<double> vals = new List<double>();
            for (int i = 0; i < matr.GetLength(0)-1; ++i)
                for (int j = 0; j < matrix.Columns.Length; ++j)
                    vals.Add(matr[i + 1, j]);
            var replacedVals = getReplacementRank(vals);
            for (int i = 0; i < matr.GetLength(0)-1; ++i)
                for (int j = 0; j < matrix.Columns.Length; ++j)
                    matr[i + 1, j] = replacedVals[matr[i + 1, j]];
            List<double> summs = new List<double>();
            for (int i = 0; i < matr.GetLength(0)-1; ++i)
            {
                double sum = 0;
                for (int j = 0; j < matrix.Columns.Length; ++j)
                    sum += matr[i + 1, j];
                summs.Add(sum);
            }
            return summs;
        }

        private Dictionary<double, double> getReplacementRank(List<double> newmatr, DataType criteria = DataType.Ascending)
        {
            Dictionary<double, double> res = new Dictionary<double, double>();
            newmatr.Sort();
            if (criteria == DataType.Ascending)
                newmatr.Reverse();
            if (newmatr.Count <= 0)
                return res;
            //Временная переменная
            double t = newmatr[0];
            //Количество повторений одного и тогоже числа значимости критериев
            int c = 1;
            for (int i = 1; i < newmatr.Count; ++i)
                if (newmatr[i].Equals(t))
                    c += 1;
                else
                {
                    double summa = 0;
                    for (int j = i - 1; j > i - 1 - c; --j)
                    {
                        summa += (j + 1);
                    }
                    summa /= c;
                    res.Add(newmatr[i - 1], summa);
                    t = newmatr[i];
                    c = 1;
                }
            double sum = 0;
            for (int j = newmatr.Count - 1; j > newmatr.Count - 1 - c; --j)
            {
                sum += (j + 1);
            }
            sum /= c;
            res.Add(newmatr[newmatr.Count - 1], sum);
            return res;
        }


        public string StepByStepSolution(TaskMatrix matrix)
        {
            return "To be continued";
        }
    }
}
