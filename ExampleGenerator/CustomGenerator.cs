﻿using CommonClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleGenerator
{
    /// <summary>
    /// Пример генератора
    /// </summary>
    public class CustomGenerator:IMatrixGenerator
    {
        #region IMatrixGenerator Members
        /// <summary>
        /// Параметры
        /// </summary>
        public Parameter[] Parameters
        {
            get {
                //Тут мы задаем нужные нам параметры
                //В этом примере мы задаем интовый параметр от 3 до 100 значение по умолчанию 4, уникальный ид cust-gen-all
                Parameter[] res = new Parameter[1];
                res[0] = new Parameter("Чем забить ВСЕ поля", 100, 3, 4, "cust-gen-all");
                return res;
            }
        }
        /// <summary>
        /// Генерация матрицы
        /// </summary>
        /// <param name="colCount">Число столбцов</param>
        /// <param name="rowCount">Число строк</param>
        /// <returns>Матрица</returns>
        public TaskMatrix Generate(int colCount, int rowCount)
        {
            //Вот так получаем нужный нам параметр
            int allPar=Parameters[0].IntegerValue;
            //Создаем матрицу
            TaskMatrix res = new TaskMatrix(MethodName,colCount);
            //Сначала добавляем столбцы
            for (int i = 1; i <= colCount; i++)
                res.AddColumn("P" + i, allPar, DataType.Ascending);
            //Потом строки
            for (int i = 1; i <= rowCount; i++)
            {
                //Сначала создаем объект строки
                ObjectRow temp = new ObjectRow("Object" + i);
                for (int j = 0; j < colCount; j++)
                    //Добавляем параметры в строку
                    temp.AddProperty(allPar);
                //Потом добавляем
                res.AddRow(temp);
            }
            //отдаем результат
            return res;
        }
        /// <summary>
        /// Название метода
        /// </summary>
        public string MethodName
        {
            //Этот текст будет использвоаться в Excel поэтому не использовать запрещенные символы
            get { return "Метод для примера"; }
        }

        #endregion
    }
}
