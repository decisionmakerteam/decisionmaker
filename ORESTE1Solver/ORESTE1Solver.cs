﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonClasses;

namespace ORESTE1Solver
{
    public class ORESTE1Solver:IMatrixSolver
    {
        private Parameter[] parameters = null;
        public Parameter[] Parameters
        {
            get
            {
                if (parameters == null)
                {
                    parameters = new Parameter[1];
                    parameters[0] = new Parameter("a(alpha)", Int32.MaxValue, 0, 0.5, 0.0001, "oreste1-a");
                }
                return parameters;
            }
        }

        public string MethodName
        {
            get { return "ORESTE I"; }
        }

        public string Solve(TaskMatrix matrix)
        {
            double[,] matr;
            List<int> deletedRows;
            //Получаем суммы по строкам объектов
            List<double> summs = SolveOresto1(matrix, out matr, out deletedRows);
            //Ранжируем суммы
            var rank = getReplacementRank(summs);
            //Выписываем ответ
            StringBuilder result = new StringBuilder("ВРО: (");
            int count_of_deleted = 0;
            for (int i = 0; i < matrix.Rows.Length; ++i)
            {
                if (deletedRows.Contains(i))
                {
                    result.Append("-");
                    count_of_deleted += 1;
                }
                else
                    result.Append(rank[summs[i - count_of_deleted]].ToString());
                result.Append(";");
            }
            result.Remove(result.Length - 1, 1);
            result.Append(")");
            return result.ToString();
        }

        /// <summary>
        /// Решает входную матрицу методом ORESTE I
        /// </summary>
        /// <param name="matrix">Матрица с весами критериев, типами критериев и значениями показателей</param>
        /// <returns>Сумма по каждому объекту в порядке следования объектов в матрице сверху вниз</returns>
        private List<double> SolveOresto1(TaskMatrix matrix, out double[,] matr, out List<int> deletingRows)
        {
            double a = Parameters[0].DoubleValue;
            //Заполняем матрицу
            matr = new double[matrix.Rows.Length+1, matrix.Columns.Length];
            for (int i = 0; i < matrix.Columns.Length; ++i)
                matr[0, i] = matrix.Columns[i].Weight;
            for (int i = 0; i < matrix.Rows.Length; i++)
                for (int j = 0; j < matrix.Columns.Length; j++)
                    matr[i + 1, j] = matrix.Rows[i].Properties[j];
            //Делаем выборку объектов оптимальной по Порето
            deletingRows = new List<int>();
            for (int i = 1; i < matr.GetLength(0); i++)
            {
                for (int j = 1; j < matr.GetLength(0); j++)
                {
                    if (i != j)
                    {
                        bool unneed = true;
                        for (int k = 0; k < matrix.Columns.Length; k++)
                        {
                            bool dominator = matr[i, k] >= matr[j, k];
                            if (matrix.Columns[k].DataType == DataType.Descending)
                                dominator = !dominator;
                            unneed = unneed && dominator;
                        }
                        if (unneed && !deletingRows.Contains(i-1) && !deletingRows.Contains(j-1))
                            deletingRows.Add(j - 1);
                    }
                }
            }
            //Убираем ненужные строки
            double[,] nmatr = new double[matrix.Rows.Length + 1 - deletingRows.Count, matrix.Columns.Length];
            for (int k = 0; k < matrix.Columns.Length; k++)
                nmatr[0, k] = matr[0, k];
            int l = 0;
            for (int i = 1; i < matr.GetLength(0); i++)
            {
                if (!deletingRows.Contains(i - 1))
                {
                    l += 1;
                    for (int k = 0; k < matrix.Columns.Length; k++)
                        nmatr[l, k] = matr[i, k];
                }
            }
            matr = nmatr;
            //Ранжируем критерии
            List<double> criterias = new List<double>();
            for (int i = 0; i < matrix.Columns.Length; ++i)
                criterias.Add(matr[0, i]);
            var replaceCriterias = getReplacementRank(criterias);
            for (int i = 0; i < matrix.Columns.Length; ++i)
                matr[0, i] = replaceCriterias[matr[0, i]];
            //Ранжируем столбцы показателей
            for (int j = 0; j < matrix.Columns.Length; ++j)
            {
                List<double> values = new List<double>();
                for (int i = 0; i < matr.GetLength(0)-1; ++i)
                    values.Add(matr[i+1, j]);
                var replacedValues = getReplacementRank(values, matrix.Columns[j].DataType);
                for (int i = 0; i < matr.GetLength(0)-1; ++i)
                    matr[i+1, j] = replacedValues[matr[i+1, j]];
            }
            //Вычисляем проекции
            for (int i = 0; i < matr.GetLength(0)-1; ++i)
                for (int j = 0; j < matrix.Columns.Length; ++j)
                    matr[i + 1, j] = (1 - a) * matr[i + 1, j] + a * matr[0, j];
            //Ранжируем все показатели
            List<double> vals = new List<double>();
            for (int i = 0; i < matr.GetLength(0)-1; ++i)
                for (int j = 0; j < matrix.Columns.Length; ++j)
                    vals.Add(matr[i + 1, j]);
            var replacedVals = getReplacementRank(vals);
            for (int i = 0; i < matr.GetLength(0)-1; ++i)
                for (int j = 0; j < matrix.Columns.Length; ++j)
                    matr[i + 1, j] = replacedVals[matr[i + 1, j]];
            List<double> summs = new List<double>();
            for (int i = 0; i < matr.GetLength(0)-1; ++i)
            {
                double sum = 0;
                for (int j = 0; j < matrix.Columns.Length; ++j)
                    sum += matr[i + 1, j];
                summs.Add(sum);
            }
            return summs;
        }

        private Dictionary<double, double> getReplacementRank(List<double> newmatr, DataType criteria = DataType.Ascending)
        {
            Dictionary<double, double> res = new Dictionary<double, double>();
            newmatr.Sort();
            if (criteria == DataType.Ascending)
                newmatr.Reverse();
            if (newmatr.Count <= 0)
                return res;
            //Временная переменная
            double t = newmatr[0];
            //Количество повторений одного и тогоже числа значимости критериев
            int c = 1;
            for (int i = 1; i < newmatr.Count; ++i)
                if (newmatr[i].Equals(t))
                    c += 1;
                else
                {
                    double summa = 0;
                    for (int j = i - 1; j > i - 1 - c; --j)
                    {
                        summa += (j + 1);
                    }
                    summa /= c;
                    res.Add(newmatr[i - 1], summa);
                    t = newmatr[i];
                    c = 1;
                }
            double sum = 0;
            for (int j = newmatr.Count - 1; j > newmatr.Count - 1 - c; --j)
            {
                sum += (j + 1);
            }
            sum /= c;
            res.Add(newmatr[newmatr.Count - 1], sum);
            return res;
        }

        public string StepByStepSolution(TaskMatrix matrix)
        {
            return "To be continued";
        }
    }
}
