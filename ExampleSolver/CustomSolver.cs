﻿using CommonClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleSolver
{
    /// <summary>
    /// Пример плагина решателя
    /// </summary>
    public class CustomSolver:IMatrixSolver
    {
        #region IMatrixSolver Members
        /// <summary>
        /// Параметры
        /// </summary>
        public Parameter[] Parameters
        {
            get
            {
                //Здесь надо указать какие параметры будут нужны вашему методу
                //В это примере задан 1 булевый параметр с текстом в клиенте Ололо? последнее уникальный идентификатор, true - значение по умолчанию)
                Parameter[] res = new Parameter[1];
                res[0] = new Parameter("Ололо?",true,"cust-solve-olo");
                return res;
            }
        }
        /// <summary>
        /// Название метода
        /// </summary>
        public string MethodName
        {
            //Этот текст будет использоваться в таблицах экселя, поэтому без запрещенный символов лучше
            get { return "Оло или Трололо"; }
        }
        /// <summary>
        /// Решение матрицы
        /// </summary>
        /// <param name="matrix">Матрица</param>
        /// <returns>Решение</returns>
        public string Solve(TaskMatrix matrix)
        {
            //Вот пример как вынуть значение из параметра если он булевый
            return Parameters[0].BooleanValue ? "Ололо" : "Трололо";
        }
        /// <summary>
        /// Пошаговое решение матрицы
        /// </summary>
        /// <param name="matrix">Матрица</param>
        /// <returns>Пошаговое решение</returns>
        public string StepByStepSolution(TaskMatrix matrix)
        {
            string res = "Каждое действие описываем и пихаем в строку результата";
            // Возвращаем наше пошаговое решение
            return res;
        }
        #endregion
    }
}
