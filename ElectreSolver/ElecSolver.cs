﻿using CommonClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectreSolver
{
    public class ElecSolver:IMatrixSolver
    {
        #region IMatrixSolver Members

        public Parameter[] Parameters
        {
            get {Parameter[] param = new Parameter[2];
                param[0] = new Parameter("Предел согласия B",1,0.5,0.75,0.001,"Коэффициент B");
                param[1] = new Parameter("Предел несогласия D",1,0,0.5,0.001,"Коэффициент D");
                return param;
            }
        }

        public string MethodName
        {
            get{
		 		return "Метод ELECTRE";
			 } 
        }

        public string Solve(TaskMatrix matrix)
        {
            Electre electre = new Electre(matrix, Parameters);
            electre.setConsent();
            electre.setDisagre();
            return  electre.setResult();
            //return String.Join(Environment.NewLine, electre.result);
        }

        public string StepByStepSolution(TaskMatrix matrix)
        {
           return "В проекте";
        }

        #endregion
    }

    /**
	В этом классе реализованы решение методом ELECTRE.
	Требуется доработать метод RemoveUneffective;
	*/
	internal class Electre{
		
		public string[,] result;			//Матрица результатов.
		
		private List<DiscoveredObjects> DOList;	//Список исследуемых объектов.
		private List<double> weight;			//Массив весов критериев.	
		private double B;					//Предел согласия.
		private double D; 				//Предел несогласия.
		//private double[] delta;
		
		internal Electre(TaskMatrix innermatrix, Parameter[] parameter){
			
			DOList = new List<DiscoveredObjects>();
			B = parameter[0].DoubleValue;
			D = parameter[1].DoubleValue;
			weight= new List<double>();
			weight=innermatrix.Columns.ToList().Select(x=>x.Weight).ToList();
			for (int i = 0; i < innermatrix.Rows.Length; i++){
				DOList.Add(new DiscoveredObjects(innermatrix.Rows[i],innermatrix.Rows.Length));
			}
			
			/*for(int i =0; i< weight.Length; i++){
				weight[i] = innermatrix.Columns[i].Weight;
			}*/

			
			for (int i = 0; i < DOList.Count; i++){
               		for (int l = 0; l < DOList.Count; l++){
               			for(int j=0; j< weight.Count; j++){
               				if(DOList[i].Param(j)>=DOList[l].Param(j)) DOList[i].Coff[l,j]=1.0;
               				else DOList[i].Coff[l,j]=0;
               			}
                		}
            	}
		}
		
		internal void setConsent(){
			for (int i = 0; i < DOList.Count; i++){
                for (int l = 0; l < DOList.Count; l++){
               			DOList[i].consent[l]=0;
               			for(int j=0; j< weight.Count; j++){
               				if(l!=i)DOList[i].consent[l] += DOList[i].Coff[l,j]*weight[j];	
               			}
                        if (DOList[i].consent[l] == 1) { RemoveUneffective(l); l--; i--; }
                		}
            	}
		}
		
		internal void setDisagre(){
			//double[,] disagre=new double[DOList.Count,DOList.Count];
			double [] delta = new double[weight.Count];	
			double max;
			double min;
			for(int i=0; i < weight.Count; i++){
				max = DOList[0].Param(i);
				min = DOList[0].Param(i);
                for (int j = 1; j < DOList.Count; j++)
                {
					if(max<DOList[j].Param(i)) max = DOList[j].Param(i);
					if(min>DOList[j].Param(i)) min = DOList[j].Param(i);
				}
				delta[i] = max - min;
			}
			for (int i = 0; i < DOList.Count; i++){
               		for (int l = 0; l < DOList.Count; l++){
               			max = 0;
                        min = 0;
               			for(int j=0; j< weight.Count; j++){
               			    if(l!=i) min = (DOList[l].Param(j) - DOList[i].Param(j))/delta[j];
               				if (min > 1) RemoveUneffective(l);
               				if (max < min) max = min;	
               			}
               			DOList[i].disagre[l] = max;
                		}
            	}
		}
		
		internal String setResult(){
			
            
            byte[,] resulter =new byte[DOList.Count,DOList.Count];
            for(int i=0;i<DOList.Count;i++){
                for(int j=0; j<DOList.Count;j++){
                    if(i==j) resulter[i,j]=0;
                    else if(DOList[i].consent[j] > B & DOList[i].disagre[j] < D) resulter[i,j]=1;
                    else resulter[i,j]=2;
                }
            }
            
            result= new string[DOList.Count+1,DOList.Count+1];
			result[0,0] = "Объекты";
			for(int i=1; i<=DOList.Count;i++){
				result[0,i]=result[i,0] = DOList[i-1].Name();
			}
			
			for(int i =1; i<=DOList.Count; i++){
				for(int j=1; j<=DOList.Count; j++){
						/*if(DOList[i-1].consent[j-1] > B & DOList[i-1].disagre[j-1] < D) result[i,j] = ">";
						else result[i,j]="<"; */
                        if(resulter[i-1,j-1]==0)
                            result[i,j]="~";
                        else
                        if(resulter[i-1,j-1]==1 & resulter[j-1,i-1]==2)
                            result[i,j] = ">";
                        else
                        if(resulter[i-1,j-1]==2 & resulter[j-1,i-1]==1)
                            result[i,j]="<";
                        else
                        if(resulter[i-1,j-1]==1 & resulter[j-1,i-1]==1)
                            result[i,j]="~";
                        else
                        if(resulter[i-1,j-1]==2 & resulter[j-1,i-1]==2)
                            result[i,j]="N";


					
				}
			}
			/*for(int i=1; i<=DOList.Count; i++) result[i,i]="*";*/
            string res="";
            for(int i =0; i<result.GetLength(0);i++){
                for (int j = 0; j < result.GetLength(1);j++ ){
                    res+=result[i,j]+"\t ";
                }
                res += "\n";
            }




            return res;
        
        }
		
		private void RemoveUneffective(int j){
			for(int i=0; i<DOList.Count; i++){
				DOList[i].Delete(j);
			}
            DOList.RemoveAt(j);
           // weight.RemoveAt(j);
			
		}
	}
	
	internal class DiscoveredObjects{
		
		private string name;
		private double[] parametrs;
		internal double[,] Coff;
		internal double[] consent;
		internal double[] disagre;
		
		internal DiscoveredObjects(ObjectRow row, int rowkol){
			this.name = row.Name;
			this.parametrs = new double[row.Properties.Length];
            this.Coff = new double[rowkol, row.Properties.Length];
            this.consent = new double[rowkol];
            this.disagre = new double[rowkol];
			for(int i=0;i<parametrs.Length;i++){
				parametrs[i]=row.Properties[i];
			}
			
		}
		
		public string Name(){
			return name;
		}
		
		public double Param(int i){
			return parametrs[i];
		}
	    public void Delete(int index){
            double[,] newCoff = new Double[Coff.GetLength(0)-1,Coff.GetLength(1)];
            double[] newConsent = new Double[Coff.GetLength(0)-1];
            double[] newDisagre = new Double[Coff.GetLength(0)-1];
            for(int i=0;i<index;i++){
                newConsent[i]= consent[i];
                newDisagre[i]=disagre[i];
                for (int j=0;j<Coff.GetLength(1);j++){
                    newCoff[i,j] = Coff[i,j];
                }
            }
            for(int i=index;i<newCoff.GetLength(0);i++){
                for (int j=0;j<Coff.GetLength(1);j++){
                    newCoff[i,j] = Coff[i+1,j];
                }
                newConsent[i] = consent[i+1];
                newDisagre[i] = disagre[i+1];
            }
            Coff = newCoff;
            disagre=newDisagre;
            consent=newConsent;
        }
	}
	

}
