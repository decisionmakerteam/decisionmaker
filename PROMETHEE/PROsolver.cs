﻿using CommonClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROMETHEE
{
    public class PROsolver : IMatrixSolver
    {
        #region IMatrixSolver Members
        /// <summary>
        /// Параметры
        /// </summary>
        public Parameter[] Parameters
        {
            get
            {
                // В данном методе необходимо 3*количество критериев параметров(номер функции,1 прараметр,2 параметр)
                Parameter[] res = new Parameter[9];
                res[0] = new Parameter("Функция критерия 1", 6, 1, 0, "func1_name");
                res[1] = new Parameter("Значение p функции 1", 100, 0, 0, "func1_p");
                res[2] = new Parameter("Значение q функции 1", 100, 0, 0, "func1_q");

                res[3] = new Parameter("Функция критерия 2", 6, 1, 0, "func2_name");
                res[4] = new Parameter("Значение p функции 2", 100, 0, 0, "func2_p");
                res[5] = new Parameter("Значение q функции 2", 100, 0, 0, "func2_q");

                res[6] = new Parameter("Функция критерия 3", 6, 1, 0, "func3_name");
                res[7] = new Parameter("Значение p функции 3", 100, 0, 0, "func3_p");
                res[8] = new Parameter("Значение q функции 3", 100, 0, 0, "func3_q");

                return res;
            }
        }
        /// <summary>
        /// Название метода
        /// </summary>
        public string MethodName
        {
            get { return "Метод PROMETHEE"; }
        }
        /// <summary>
        /// Решение матрицы
        /// </summary>
        /// <param name="matrix">Матрица</param>
        /// <returns>Решение</returns>
        public string Solve(TaskMatrix matrix)
        {
            string res = "";
            PRO myPRO = new PRO(matrix.Rows.Length, matrix.Columns.Length, Parameters[0].IntegerValue, Parameters[1].IntegerValue, Parameters[2].IntegerValue, Parameters[3].IntegerValue, Parameters[4].IntegerValue, Parameters[5].IntegerValue, Parameters[6].IntegerValue, Parameters[7].IntegerValue, Parameters[8].IntegerValue);
            myPRO.fillMatr(matrix.Columns, matrix.Rows);
            myPRO.cleanMatr();
            res = myPRO.Writeans(myPRO.order);
            return res;
        }

        public string StepByStepSolution(TaskMatrix matrix)
        {
            //можно сделать пошаговый вывод результата метода CalcCriteria
            string res = "";

            return res;
        #endregion
        }
    }
}
