﻿using CommonClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PROMETHEE
{
    public class PRO
    {
        public List<int> exist = new List<int>(); // Оставшиеся объекты
        public List<int> rang = new List<int>(); // Объекты с присвоенным рангом
        public int[,] matr; // Матрица показателей
        public double[] weights; // Веса
        public int[,] func = new int[critcount, 3]; // Матрица функций
        const int critcount = 3;// количетво критериев
        public double[][,] critmas = new double[critcount][,];
        public double[,] preferences;
        public double[] directpref;
        public double[] oppositepref;
        public int[,] order;
        public double[] pref;
        public double[] orgerpref;
        public bool[] incDec; // Возрастающий / уменьшающийся рейтинг
        public void fillMatr(CommonClasses.ObjectColumn[] objectColumn, CommonClasses.ObjectRow[] objectRow)
        {
            for (int i = 0; i < objectColumn.Length; i++)
            {
                incDec[i] = objectColumn[i].DataType.Equals(DataType.Ascending);
                weights[i] = objectColumn[i].Weight;
            }
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    matr[i, j] = (int)objectRow[i].Properties[j];
                }
            }
        }
        internal void cleanMatr()
        {
            List<int> removeLines = new List<int>();
            for (int i = 0; i < this.matr.GetLength(0); i++)
            {
                bool needTOClear = false;
                bool strictlyLess = false;
                for (int j = 0; j < this.matr.GetLength(0); j++)
                {
                    if (i != j)
                    {
                        bool menshe = true;
                        for (int k = 0; k < this.matr.GetLength(1); k++)
                        {
                            if (incDec[k]) // Если увеличивается, то сравниваем какой больше
                            {
                                menshe = menshe && (this.matr[i, k] <= this.matr[j, k]);
                                strictlyLess = strictlyLess || (this.matr[i, k] < this.matr[j, k]);
                            }
                            else // инчае, то сравниваем какой меньше
                            {
                                menshe = menshe && (this.matr[i, k] >= this.matr[j, k]);
                                strictlyLess = strictlyLess || (this.matr[i, k] > this.matr[j, k]);
                            }
                        }
                        needTOClear = needTOClear || menshe && strictlyLess;

                    }
                }
                if (needTOClear)
                    removeLines.Add(i);
            }
            removeLines.Reverse();
            foreach (int i in removeLines)
            {
                this.exist.RemoveAt(i);
                this.deleteLine(i);
            }

            for (int i = 0; i < critcount; i++)
            {
                critmas[i] = new double[matr.GetLength(0), matr.GetLength(0)];
            }
            CalcCriteria(matr, 1);
            CalcCriteria(matr, 2);
            CalcCriteria(matr, 3);
            preferences = new double[matr.GetLength(0), matr.GetLength(0)];
            directpref = new double[matr.GetLength(0)];
            oppositepref = new double[matr.GetLength(0)];
            order = new int[matr.GetLength(0), matr.GetLength(0)];
            pref = new double[matr.GetLength(0)];
            orgerpref = new double[matr.GetLength(0)];
            CalcPreferencies(critmas, weights);
            Prom1(directpref, oppositepref);
            Prom2(directpref, oppositepref);

        }
        private void deleteLine(int del)
        {
            int shift = 0;
            int[,] res = new int[matr.GetLength(0) - 1, matr.GetLength(1)];
            for (int i = 0; (i < res.GetLength(0)); i++)
            {
                if (i == del)
                    shift++;
                for (int j = 0; j < matr.GetLength(1); j++)
                    res[i, j] = matr[i + shift, j];
            }
            matr = res;
        }
        private double func_1(int d) //константа
        {
            double result = (d > 0) ? 1 : 0;
            return result;
        }
        private double func_2(int d, int q)//-- хз как называется
        {
            double result = (d > q) ? 1 : 0;
            return result;
        }
        private double func_3(int d, int p)//-- хз как называется
        {
            double result = 0;
            if (d < 0)
                result = 0;
            if ((d >= 0) & (d <= p))
                result = d / p;
            if (d > p)
                result = 1;
            return result;
        }
        private double func_4(int d, int p, int q)//-- хз как называется
        {
            double result = 0;
            if (d < q)
                result = 0;
            if ((d >= q) & (d <= p))
                result = 1 / 2;
            if (d > p)
                result = 1;
            return result;
        }
        private double func_5(int d, int p, int q)//-- хз как называется
        {
            double result = 0;
            if (d < q)
                result = 0;
            if ((d >= q) & (d <= p))
                result = (d - p) / (p - q);
            if (d > p)
                result = 1;
            return result;
        }
        private double func_6(int d, int s)//экспонента
        {
            double result = (d < 0) ? 0 : (1 - Math.Exp(-d * d / (2 * s * s)));

            return result;
        }
        /// <summary>
        /// расчет значений для таблицы критерия
        /// </summary>
        /// <param name="matr">исходная матрица</param>
        /// <param name="critnum">номер критерия</param>
        public void CalcCriteria(int[,] matr, int critnum)
        {
            for (int i = 0; i < this.matr.GetLength(0); i++)
            {
                for (int j = 0; j < this.matr.GetLength(0); j++)
                    critmas[critnum - 1][i, j] = function(delta(matr[j, critnum - 1], matr[i, critnum - 1], incDec[critnum - 1]), critnum - 1);
            }
        }
        /// <summary>
        /// расчет значений для таблицы критерия
        /// </summary>
        /// <param name="matr">исходная матрица</param>
        /// <param name="critnum">номер критерия</param>
        public void CalcPreferencies(double[][,] critmas, double[] weights)
        {
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(0); j++)
                {
                    for (int k = 0; k < critcount; k++)
                    {
                        preferences[i, j] += weights[k] * critmas[k][i, j];
                    }
                }
            }

            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(0); j++)
                {
                    oppositepref[i] += preferences[i, j];
                }
            }

            for (int j = 0; j < matr.GetLength(0); j++)
            {
                for (int i = 0; i < matr.GetLength(0); i++)
                {
                    directpref[j] += preferences[i, j];
                }
            }
        }

        public void Prom1(double[] directpref, double[] oppositepref)
        {
            for (int i = 0; i < directpref.Length; i++)
            {
                for (int j = 0; j < oppositepref.Length; j++)
                {
                    if (order[i, j] == 0)
                    {
                        if ((directpref[i] > directpref[j] && oppositepref[i] < oppositepref[j]) || (directpref[i] > directpref[j] && oppositepref[i] == oppositepref[j]) || (directpref[i] == directpref[j] && oppositepref[i] < oppositepref[j]))
                        {
                            order[i, j] = 1;
                            order[j, i] = -1;
                        }
                        else if (directpref[i] == directpref[j] && oppositepref[i] == oppositepref[j])
                        {
                            order[i, j] = 0;
                        }
                        else
                        {
                            order[i, j] = 2;
                            order[j, i] = 2;
                        }
                    }
                }
            }
        }
        public void Prom2(double[] directpref, double[] oppositepref)
        {
            for (int i = 0; i < directpref.Length; i++)
            {
                pref[i] = directpref[i] - oppositepref[i];
            }
            for (int i = 0; i < pref.Length; i++)
            {
                orgerpref[i] = pref[i];
            }
            Array.Sort(orgerpref);
            Array.Reverse(orgerpref);
        }
        /// <summary>
        /// расчет значения разности элеметов для критерия
        /// </summary>
        /// <param name="e1">значения того параметра, с которым сранивают</param>
        /// <param name="e2">значения того параметра, с который сранивают</param>
        /// <param name="par"></param>
        /// <returns></returns>
        private int delta(int e1, int e2, bool par)
        {
            return (par) ? (e1 - e2) : (e2 - e1);
        }
        /// <summary>
        /// выбор функции по входному параметру и расчет ее значения
        /// </summary>
        /// <param name="delta">разность между элементами слобца для конкретного критерия</param>
        /// <param name="critn">номер сторбца или критерия для расчетов</param>
        /// <returns>значение функции для критерия</returns>

        private double function(int delta, int critn)
        {
            double res = 0;
            switch (func[critn, 0])
            {
                case 1:
                    res = func_1(delta);
                    break;
                case 2:
                    res = func_2(delta, func[critn, 1]);
                    break;
                case 3:
                    res = func_3(delta, func[critn, 1]);
                    break;
                case 4:
                    res = func_4(delta, func[critn, 1], func[critn, 2]);
                    break;
                case 5:
                    res = func_5(delta, func[critn, 1], func[critn, 2]);
                    break;
                case 6:
                    res = func_6(delta, func[critn, 1]);
                    break;
            }
            return res;
        }
        public string Writeans(int[,] order)
        {
            String res = "";
            res += "PROMETHEE I" + "\n";
            res += "     ";
            for (int i = 0; i < exist.Count; i++)
            {
                res += "B" + (exist[i] + 1) + " ";
            }
            res += "\n";
            for (int i = 0; i < order.GetLength(0); i++)
            {
                res += "B" + (exist[i] + 1) + " ";
                for (int j = 0; j < order.GetLength(1); j++)
                {
                    switch (order[i, j])
                    {
                        case 1:
                            res += ">" + "   ";
                            break;
                        case -1:
                            res += "<" + "   ";
                            break;
                        case 2:
                            res += "N" + "   ";
                            break;
                        case 0:
                            res += "~" + "   ";
                            break;
                    }
                }
                res += "\n";
            }
            res += "PROMETHEE II" + "\n";
            for (int i = 0; i < exist.Count; i++)
            {
                res += "B" + (exist[i] + 1) + "   ";
            }
            res += "\n";
            string res1 = "";
            string res2 = "";
            for (int i = 0; i < pref.Length; i++)
            {
                for (int j = 0; j < orgerpref.Length; j++)
                {
                    if (pref[i] == orgerpref[j])
                    {
                        res1 += pref[i] + " ";
                        res2 += (j + 1) + "    ";
                    }
                }
            }
            res += res1;
            res += "\n";
            res += res2;
            return res;
        }
        // конструктор
        public PRO(int n, int m, int f1, int p1, int q1, int f2, int p2, int q2, int f3, int p3, int q3)
        {
            matr = new int[n, m];
            weights = new double[m];
            incDec = new bool[m];
            func = new int[m, critcount];
            func[0, 0] = f1;
            func[0, 1] = p1;
            func[0, 2] = q1;
            func[1, 0] = f2;
            func[1, 1] = p2;
            func[1, 2] = q2;
            func[2, 0] = f3;
            func[2, 1] = p3;
            func[2, 2] = q3;


            for (int i = 0; i < matr.GetLength(0); i++)
                exist.Add(i);
        }
    }
}