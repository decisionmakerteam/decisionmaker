﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManualCreateTableGenerator
{
    public partial class mform : Form
    {
        public mform()
        {
            InitializeComponent();
        }

        internal DialogResult ShowDialog(int colCount, int rowCount)
        {
            grid.Columns.Add("Тип критерия", "");
            for (int i = 1; i <= colCount; i++)
                grid.Columns.Add("Критерий " + i.ToString(), "Критерий " + i.ToString());
            //Создание типов критериев
            DataGridViewRow ct = new DataGridViewRow();
            ct.Cells.Add(createTBCell("Тип критерия"));
            string[] names = Enum.GetNames(typeof(CommonClasses.DataType));
            for (int i = 1; i <= colCount; i++)
                ct.Cells.Add(createCBCell(names));
            grid.Rows.Add(ct);
            //Создание весов критериев
            ct = new DataGridViewRow();
            ct.Cells.Add(createTBCell("Вес критерия"));
            for (int i = 1; i <= colCount; i++)
                ct.Cells.Add(createDoubleCell(0));
            grid.Rows.Add(ct);
            //Создание объектов
            for (int j = 1; j <= rowCount; j++)
            {
                ct = new DataGridViewRow();
                ct.Cells.Add(createTBCell("Объект " + j.ToString()));
                for (int i = 1; i <= colCount; i++)
                {
                    ct.Cells.Add(createDoubleCell(1.5));
                }
                grid.Rows.Add(ct);
            }
            return this.ShowDialog();
        }

        private DataGridViewTextBoxCell createTBCell(String text)
        {
            DataGridViewTextBoxCell res = new DataGridViewTextBoxCell();
            res.ValueType = typeof(string);
            res.Value = text;
            return res;
        }

        private DataGridViewTextBoxCell createDoubleCell(double dbl)
        {
            DataGridViewTextBoxCell res = new DataGridViewTextBoxCell();
            res.ValueType = typeof(double);
            res.Value = dbl;
            return res;
        }

        private DataGridViewComboBoxCell createCBCell(string[] text)
        {
            DataGridViewComboBoxCell res = new DataGridViewComboBoxCell();
            res.ValueType = typeof(string);
            res.Items.AddRange(text);
            res.Value = text[0];
            return res;
        }

        private void grid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Таблица заполнена некорректно. Проверьте и исправьте, пожалуйста");
        }
    }
}
