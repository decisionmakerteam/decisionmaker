﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonClasses;
using System.Windows.Forms;

namespace ManualCreateTableGenerator
{
    public class ManualCreateTableGeneratorer:IMatrixGenerator
    {
        private Parameter[] parameters = new Parameter[0];
        
        public TaskMatrix Generate(int colCount, int rowCount)
        {
            mform form = new mform();
            DialogResult ds = form.ShowDialog(colCount, rowCount);
            return (ds == DialogResult.OK) ? getMatrix(form) : null;
        }

        private TaskMatrix getMatrix(mform form)
        {
            TaskMatrix m = new TaskMatrix("ManualTable", form.grid.Columns.Count - 1);
            for (int i = 1; i <= form.grid.Columns.Count-1; ++i)
                m.AddColumn(new ObjectColumn(form.grid.Columns[i].HeaderText, (double)form.grid.Rows[1].Cells[i].Value, (DataType)Enum.Parse(typeof(DataType), (string)form.grid.Rows[0].Cells[i].Value)));
            for(int i=0; i<form.grid.Rows.Count-2; ++i)
            {
                ObjectRow or = new ObjectRow((string)form.grid.Rows[i+2].Cells[0].Value);
                for (int j = 1; j <= form.grid.Columns.Count-1; j++)
                    or.AddProperty((double)form.grid.Rows[i + 2].Cells[j].Value);
                m.AddRow(or);
            }
            return m;
        }

        Parameter[] IMatrixGenerator.Parameters
        {
            get
            {
                //Для создания матрицы параметры не нужны
                return parameters;
            }
        }

        string IMatrixGenerator.MethodName
        {
            get
            {
                return "Создать матрицу вручную";
            }
        }
    }
}
